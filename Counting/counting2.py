#!/usr/bin/env python3
"""
Author: Xirui LIU
Description:


"""
import os

def count_files(folder_path, prefixes):
    for prefix in prefixes:
        count = 0
        for subfolder_name in os.listdir(folder_path):
            if subfolder_name.startswith(prefix) and os.path.isdir(os.path.join(folder_path, subfolder_name)):
                subfolder_path = os.path.join(folder_path, subfolder_name)
                for filename in os.listdir(subfolder_path):
                    if filename.endswith('T_RGB.tif'):
                        count += 1
                print(f"Number of files in '{subfolder_name}': {count}")
                count = 0

if __name__ == "__main__":
    folder_path = r'/lustre/backup/WUR/ABGC/liu253/Kingfish_4timepoints'
    prefixes = ['2022-02', '2022-03', '2022-04', '2022-05']

    count_files(folder_path, prefixes)