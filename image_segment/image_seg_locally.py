#!/usr/bin/env python3
"""
Author: Xirui LIU
Description: This script is to achieve image segmentation for individual
fish identification project.Could be used for all the time point image.

The image will be segmented and only Fish object remain. The ratio and margins
of the image could be set in this script

package: cv2

* There is a problem running this script in server. Recommend to run this script
locally. And upload the segmented image to server.
This script is online version
"""


import cv2
import numpy as np
import os
import traceback

def image_segmentation(image_path:str):
    """This function achieves to find the main object in each image and returns
     the location.

     Finds the edge and main contours of the input image. And finally, return
     the coordinates of the top-left corner of the minimum bounding and the
     width and height of the rectangle.
    """

    image_cropped = cv2.imread(image_path, cv2.IMREAD_COLOR)
    height, width = image_cropped.shape[:2]
    image = image_cropped[75 : height - 37, 180 : width]

    hls_image = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
    s_channel = hls_image[:, :, 2]

    s_channel_array = s_channel.flatten()

    hist_, bins = np.histogram(s_channel_array, bins=256, range=(0, 256))

    blurred = cv2.GaussianBlur(s_channel, (5, 5), 0)
    _, thresholded = cv2.threshold(blurred, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    #cv2.THRESH_OTSU indicates that the threshold value will be determined
    #automatically using Otsu's method.
    edged = cv2.Canny(thresholded, 100, 250)

    contours, _ = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    main_contour = max(contours, key=cv2.contourArea)

    x, y, w, h = cv2.boundingRect(main_contour)
    #x and y are the coordinates of the top-left corner of the minimum bounding
    #rectangle, and w is the width, h is the height of this rectangle

    # Return the (x,y) in original image
    x_original = x + 180
    y_original = y + 75

    # image_copy = image_cropped.copy()
    # cv2.rectangle(image_copy, (x_original, y_original),
    #               (x_original + w, y_original + h), (0, 255, 0), 2)
    #
    # cv2.imshow("Original Image", image_cropped)
    # cv2.imshow('edge', edged)
    # cv2.imshow("Image with Main Object", image_copy)
    # #
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    return x_original,y_original,w,h

def resize_relocate(rio: float, x_cor:int, y_cor:int, width:int, height:int, margins:int):
    """"""
    ratio = rio

    #Calculate for the center coordinates
    center_x = x_cor + width // 2
    center_y = y_cor + height // 2

    #Calculate for new width and height
    if width > height:
        new_w = width + margins * 2
        new_h = int(new_w * ratio)
    else:
        new_h = height + margins * 2
        new_w = int(new_h * ratio) #new_w = int(new_h / ratio)

    # Calculate for new (x, y) of the top left corner
    new_x = center_x - new_w // 2
    new_y = center_y - new_h // 2

    return new_x, new_y, new_w, new_h


#___Below is the main work____________________________________________________
workpath = r'March_copy'
ratio = 1/2
margins = 30
print('Start image segmentation')
# id_list = []
# id_noerror = []
for folder_name in ['train','validation']:
    folder_path = os.path.join(workpath, folder_name)
    for idname in os.listdir(folder_path):
        # id_list.append(idname)
        idpath = os.path.join(folder_path, idname)
        for filename in os.listdir(idpath):
            if filename.endswith('RGB.tif'):
                image_path = os.path.join(idpath, filename)

                x, y, w, h = image_segmentation(image_path)
                newx, newy, neww, newh = resize_relocate(ratio, x, y, w, h, margins)
                #print(newx,newy,neww, newh)

                image = cv2.imread(image_path, cv2.IMREAD_COLOR)
                #
                h_max, w_max = image.shape[:2]
                if newy + newh > h_max:
                    newh = h_max - newy

                if newx + neww > w_max:
                    neww = w_max - newx

                segment_image = image[newy:newy + newh, newx:newx + neww]
                # cv2.imshow("Image cut", segment_image)
                # cv2.waitKey(0)
                # cv2.destroyAllWindows()
                #
                save_path = os.path.join(idpath, filename)
                print(save_path)
                #cv2.imwrite(save_path, segment_image)

                try:
                    cv2.imwrite(save_path, segment_image)
                except cv2.error as e:
                    error_traceback = traceback.format_exc()
                    file_name = \
                    error_traceback.split('File "', 1)[1].split('"', 1)[0]
                    print("Error occurred in file:", file_name)
                    print("Error details:\n", error_traceback)

print(f'Image segmentation for {workpath} complete')
print('Running completely')


#____________Below is try to find the error image._____________________________
# id_setlist = list(set(id_list))
# print(len(id_setlist))
#
# with open('id_error.txt', 'r') as file:
#     for line in file:
#         line = line.strip()  # 去除行首尾的空白字符
#         if not line.startswith("February_copy"):
#             continue  # 跳过不以指定开头的行
#
#         # 提取ID部分
#         id_part = line.split("\\")[2]
#         id_noerror.append(id_part)
# missing_ids = set(id_setlist) - set(id_noerror)
#
# id_noerror_set = list(set(id_noerror))
#
# # 打印结果
# print("No Error IDs:")
# print(id_noerror_set)
# print(len(id_noerror_set))
# print("Missing IDs:")
# print(missing_ids)

#___________________________________________________________________________

#
# x, y, w, h = image_segmentation('test_0419_slant_RGB.tif')
# ratio = 1/2
# margins = 30
# new_x, new_y, new_w, new_h = resize_relocate(ratio,x, y, w, h, margins)
#
# image_ = 'test_0419_slant_RGB.tif'
# image_cropped = cv2.imread(image_, cv2.IMREAD_COLOR)
# # height, width = image_cropped.shape[:2]
# # image_test = image_cropped[75 : height - 37, 180 : width]
# cv2.rectangle(image_cropped, (new_x, new_y), (new_x + new_w, new_y + new_h), (0, 255, 0), 2)
#
# #seg_image = image_cropped[new_y:new_y+new_h, new_x:new_x+new_w]
#
# cv2.imshow("Image with resize rectangle", image_cropped)
# #cv2.imshow("Image cut", seg_image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()