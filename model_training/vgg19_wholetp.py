#!/usr/bin/env python3
"""
Author: Xirui LIU
Description:
This script aim to construct a VGG19 model for feature extraction, it should be
run with conda environment which include python version = 3.8.5, tensorflow=2.3.0
dataset is timepoint 02,03,04, and image was segmented.
05 timepoint was used as test data set.


"""

import os
import shutil
import pandas as pd

# import model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras import layers
import tensorflow as tf

# callback
from tensorflow.keras.callbacks import TensorBoard

#Construct VGG19 model
def data_augmentation():
    """"""
    # trainset = ImageDataGenerator(rescale=1.0/255., rotation_range=40,
    #                                    width_shift_range=0.2,
    #                                    height_shift_range=0.2,
    #                                    shear_range=0.2,
    #                                    zoom_range=0.2,
    #                                    horizontal_flip=True)

    trainset = ImageDataGenerator(rescale=1.0 / 255.)
    validationset = ImageDataGenerator(rescale=1.0 / 255.)
    testset = ImageDataGenerator(rescale=1.0 / 255.)

    return trainset, validationset, testset

if __name__ == "__main__":

    # dataset_cut2 = r'/lustre/backup/WUR/ABGC/liu253/Dataset/February'
    dataset_cut2 = r'/lustre/backup/WUR/ABGC/liu253/Dataset_for_Model/whole_tp_model'
    train_dir = os.path.join(dataset_cut2, 'train')
    validation_dir = os.path.join(dataset_cut2, 'validation')
    test_dir = os.path.join(dataset_cut2, 'test')



    train_datagen, validation_datagen, test_datagen= data_augmentation()

    batchsize = 32
    classmode = 'categorical'
    train_generator = train_datagen.flow_from_directory(train_dir,
                                                        batch_size=batchsize,
                                                        class_mode='categorical',
                                                        target_size=(224, 224))
    validation_generator = validation_datagen.flow_from_directory(validation_dir,
                                                            batch_size=batchsize,
                                                            class_mode='categorical',
                                                            target_size=(224, 224))
    test_generator = test_datagen.flow_from_directory(test_dir,
                                                      batch_size=batchsize,
                                                      class_mode='categorical',
                                                      target_size=(224, 224))
    base_model = VGG19(input_shape=(224, 224, 3),
                       include_top=False,
                       # for feature extraction, try false first
                       weights='imagenet')

    for layer in base_model.layers:
        layer.trainable = False

    # Flatten the output layer to one-dimensional
    x = layers.Flatten()(base_model.output)

    # Add a fully connected layer with 512 hidden units and a ReLU activation
    dense_set = 512
    x = layers.Dense(dense_set, activation='relu')(x)

    # Add drop out rate, as 0.2
    dropoutrate = 0.0
    #x = layers.Dropout(dropoutrate)(x)

    # Add the last sigmoid layer, the number of output nodes is the number of categories
    file_num = len(os.listdir(train_dir))
    x = layers.Dense(file_num, activation='softmax')(x)

    model = tf.keras.models.Model(base_model.input, x)

    learningrate = 0.0001
    model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=learningrate),
                  loss='categorical_crossentropy',
                  metrics=['acc'])

    steps_perepoch = ((2*file_num) // batchsize) + 1
    epochs_set = 100
    vgghist = model.fit(train_generator,
                        validation_data=validation_generator,
                        steps_per_epoch=steps_perepoch,
                        epochs=epochs_set)

    print('Parameters: batchsize, classmode, dense, dropout rate, lr, '
          'steps per epoch, epochs')
    print(f'{batchsize}, {classmode}, {dense_set}, {dropoutrate}, '
          f'{learningrate}, {steps_perepoch}, {epochs_set}')

    history = vgghist.history
    print(history)

    df_45 = pd.DataFrame(history)
    df_45.to_csv('vgg19_history_wholetp_0627_noaug.csv', index=False)
