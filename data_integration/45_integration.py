#!/usr/bin/env python3
"""
Author: Xirui LIU
Description:
This script is to achieve merge of April and May Fish image data.

Integrate the data sets for two time points in April and May. Collect all the
ID information and corresponding photo information in the measurements files
in each folder in the raw data. the photos collected by the F camera will
be removed.
Create two folders train and validation in work path.
Create corresponding folders in train and validation under work path based on
all the collected ID information.
The photos will be copied and moved to the folder corresponding to the ID.
Photos collected by the F camera will be ignored.

"""

import os
import csv
import shutil

def findread_measurements(datapath: str,mergefile:str,alldata:str,workpath:str):
    """

    :return: dictionary: {filename: Fish ID}
    """
    measure_pathlist = []
    for root, dir, files in os.walk(datapath):
        for file in files:
            if file.startswith('Measurements'):
                filepath = os.path.join(root, file)
                measure_pathlist.append(filepath)

    mergefile_path = os.path.join(workpath, mergefile)
    alldata_path = os.path.join(workpath, alldata)
    if os.path.exists(mergefile_path) and os.path.exists(alldata_path):
        return
    else:
        idlist = []
        allline = []
        for file_path in measure_pathlist:
            with open(file_path, 'r') as f:
                lines = f.readlines()
                if len(lines) > 2:
                    for line in lines[2:]:
                        items = line.strip().split('\t')
                        if items[4]:
                            idlist.append(items)
                    allline.extend([line.strip().split('\t') for line in lines[2:]])
                else:
                    print(
                        f"File '{file_path}' does not have enough lines.")
        print(f"There are {len(idlist)} fish have ID during all timepoints")
        print(f"There are around {len(allline)-len(idlist)} fish without ID")

    header = 'Number\tTimestamp\tImageFront\tImageTop\tTagID\tScaleWeight\tClass\tProcessing time\tSpecimen comment\tarea\tarea without caudal fin\tperimeter\ttotal length\tstandard length\tbody depth\theight\tdiameter transition point\tvolume\tweight'
    with open(mergefile_path, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header.split('\t'))
        writer.writerows(idlist)

    with open(alldata_path, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(header.split('\t'))
        writer.writerows(allline)

def create_iddict(idsfile:str, workpath:str):
    """"""
    idsdict = {}
    idspath = os.path.join(workpath,idsfile)
    with open(idspath, 'r') as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip()
            if line.startswith('Number'):
                continue
            items = line.split(',')
            timestamp, imagef, imaget, tagid = items[1:5]
            if tagid in idsdict:
                idsdict[tagid].append(imagef)
                idsdict[tagid].append(imaget)
            else:
                idsdict[tagid] = [imagef, imaget]

    # for ids, images in idsdict.items():
    #     print(f'TagID: {ids}')
    #     print(images)
    #     print(len(images))
    #     print('---')
    # print(len(idsdict))

    return idsdict

def write_idcsv(id_dict: dict, workpath: str, filename:str):
    """"""
    header = ['ID', 'image_names', 'image_number']
    csvfile = os.path.join(workpath,filename)
    if os.path.exists(csvfile):
        return
    else:
        with open(csvfile, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(header)

            for tag_id, image_list in id_dict.items():
                image_names = ','.join(image_list)
                image_num = len(image_list)
                writer.writerow([tag_id,image_names,image_num])

def create_idfolder(id_dict:dict, workpath:str, timepoints:list):
    """Create id folder to destination dir

    id_dict: {id:[imagef, imaget, ....], id:[]}
    workpath: the path of folder which store dataset for model training
    timepoints: list, store start of timepoint that  needed to build the model,
        example: ['2022-02','2022-04','2022-05']

    ***This function have been merged into move_images***
    """
    idpath_limit = []
    for tag_id, image_list in id_dict.items():
        for tp in timepoints:
            if any(image_name.startswith(tp) for image_name in image_list):
                idfolder_name = tag_id.replace(' ', '')
                idfolder_path = os.path.join(workpath, idfolder_name)
                os.makedirs(idfolder_path, exist_ok=True)
                idpath_limit.append(idfolder_path)
    return list(set(idpath_limit))

def move_images(id_dict: dict, workpath: str, timepoint: list, datapath: str):
    """
    id_dict: {id:[imagef, imaget, ....], id:[]}
    workpath: the path of folder which store dataset for model training or
        validation
    timepoints: list, store start of timepoint that  needed to build the model,
        example: ['2022-02','2022-04','2022-05']
    Datapath: folder store raw data
    """
    for tag_id, image_list in id_dict.items():
        for tp in timepoint:
            for image_name in image_list:
                if image_name.startswith(tp):
                    idfolder_name = tag_id.replace(' ', '')
                    idfolder_path = os.path.join(workpath, idfolder_name)
                    os.makedirs(idfolder_path, exist_ok=True)

                    folderpath_list = find_folders_with_tp(datapath, timepoint)
                    for folder_path in folderpath_list:
                        image_file = find_image(folder_path, image_name)
                        if image_file and image_file.endswith('_T_RGB.tif'):
                            new_image_path = os.path.join(idfolder_path, os.path.basename(image_file))
                            shutil.copy(image_file, new_image_path)


def find_folders_with_tp(datapath: str, timepoint: list):
    folderpath_list = []
    for folder_name in os.listdir(datapath):
        if any(folder_name.startswith(tp) for tp in timepoint) and \
                os.path.isdir(os.path.join(datapath, folder_name)):
            folderpath_list.append(os.path.join(datapath, folder_name))
    return folderpath_list

def find_image(folder_path, image_name):
    for file_name in os.listdir(folder_path):
        if file_name == image_name:
            return os.path.join(folder_path, file_name)


if __name__ == "__main__":
    print('start')
    rawdata = r'/lustre/backup/WUR/ABGC/liu253/Kingfish_4timepoints'
    data_summary = r'/lustre/backup/WUR/ABGC/liu253'

    #Create folder to store images of timepoint April and May
    dataset_45 = r'/lustre/backup/WUR/ABGC/liu253/Dataset/May'
    if not os.path.exists(dataset_45):
        os.makedirs(dataset_45)
        print(f'{dataset_45} have been created')

    #Go through all folder in rawdata folder, read measurements files and pick
    #up images with ID, rewrite them as csv file.
    filelist = os.listdir(rawdata)
    findread_measurements(rawdata, 'All_Measurements_ID.csv', 'All_Measurements.csv',
                          data_summary)
    idsdict = create_iddict('All_Measurements_ID.csv', data_summary)
    print(f'All_Measurements_ID.csv have been created in {data_summary}')

    ## Check the content of ids dict:
    # for ids, images in idsdict.items():
    #     print(f'TagID: {ids}')
    #     print(images)
    #     print(len(images))
    #     print('---')
    # print(len(idsdict))

    write_idcsv(idsdict, data_summary, 'id_image.csv')
    print(f'id_image.csv have been created in {data_summary}')

    # create id folder in dataset_45:
    trainset = os.path.join(dataset_45, 'train')
    validationset = os.path.join(dataset_45, 'validation')
    if not os.path.exists(trainset):
        os.makedirs(trainset)

    if not os.path.exists(validationset):
        os.makedirs(validationset)
    print(f'{trainset} and {validationset} created')

    # Indicate which timepoint images we need pick up for model construction
    timepoints45 = ['2022-05']

    # Copy and Move the imageT into their related id folder
    move_images(idsdict, trainset, timepoints45, rawdata)
    print('--------------------------')
    print(f'id folders for {timepoints45} have been create and images have '
          f'been moved completely to {trainset}')

    move_images(idsdict, validationset, timepoints45, rawdata)
    print('--------------------------')
    print(f'id folders for {timepoints45} have been create and images have '
          f'been moved completely to {validationset}')